package ro.utcn.pt.assignment2.back;

import java.time.LocalTime;
import java.util.Random;
import java.util.UUID;

/**
 *  This Class implements the Client. It gives the client and id and a random service time.
 */

public class Client {
    long id;
    LocalTime arrivalTime;
    int serviceTime;

    /**
	 * Default Client constructor
	 * @param arrivalTime - The arrival time of the client
	 * */

    public Client(LocalTime arrivalTime){
    	long num =(UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE);

    	this.id = (long) (num / Math.pow(10, Math.floor(Math.log10(num)) - 4 + 1));
        this.arrivalTime = arrivalTime;
        
        Random r = new Random();
    	this.serviceTime = r.nextInt(3) + 2;
    }

/**
 * Gets the clients ID
 * @return id of client
 * */
	public long getId() {
		return id;
	}
/**
 * Gets the time when the Client arrives
 * @return arrivalTime
 * */
	public LocalTime getArrivalTime() {
		return arrivalTime;
	}

	/**
	 * How much time does it take to process the client's needs
	 *
	 * @return The amount of time a client takes to being processed
	 * */
	public int getServiceTime() {
		return serviceTime;
	}

	/**
	 * Sets a preferred service time (OPTIONAL)
	 *
	 * */
	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	/**
	 *Client details
	 *
	 * @return Client's details
	 * */
	public String toString() {
		return "Client with id: " + id + " AT: " +arrivalTime + " ST: " + serviceTime;
	}
}
