package ro.utcn.pt.assignment2.back;


import ro.utcn.pt.assignment2.front.ApplicationUI;

import javax.swing.*;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
*   The Store class generates all the ServiceProcessors and the clients. It notifies the Log.
*/

public class Store implements Runnable {
    private final ApplicationUI ui;
    public final AtomicBoolean cancelationFlag;
    ArrayList<ServiceProcessor> queues = new ArrayList<>();
    public BlockingQueue<Client> clients = new ArrayBlockingQueue<Client>(50);

    /**
     *Constructor of Store
     *
     * @param queues - how many queues to be generated
     * @param nrOfClients - how many clients to be generated
     * @param ui - object of the GUI
     * @param cancelationFlag - allows to run thread
     * @throws InterruptedException - InterruptedException
     * */
    public Store(int queues, int nrOfClients, ApplicationUI ui, AtomicBoolean cancelationFlag) throws InterruptedException {
        generateQueues(queues);
        generateClients(nrOfClients, false);
        this.ui = ui;
        this.cancelationFlag = cancelationFlag;
    }

    /**
     *Generates Queues
     * @param nrOfQueues - number of ServiceProcessors to exist
     * */
    private void generateQueues(int nrOfQueues) {
        for (int i = 1; i <= nrOfQueues; i++) {
            queues.add(new ServiceProcessor("Q" + i, clients, this));
        }
    }

    /**
     *Gets the info to the log
     *
     * @param log - Log object
     *
     * */
    public void GetLog(Log log) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                ui.react(log);
            }
        });
    }
    /**
     *Generates Clients
     *
     * @param nrOfClients - number of clients to exist in queue
     * @param sleep - show much time the thread sleeps
     * */
    public synchronized void generateClients(int nrOfClients, boolean sleep) throws InterruptedException {
        for(int i = 0 ; i < nrOfClients; i++) {
            Client c = new Client(LocalTime.now());
            clients.put(c);
            if (sleep) {
                Thread.sleep((int)(Math.random() * 3) + 100);
            }
        }
    }

    /**
     *The threads start as long as cancelationFlag is true
     *
     * */
    public void run() {
        for(ServiceProcessor sp : queues){
            new Thread(sp).start();
        }

        while (cancelationFlag.get()){
            try {
                synchronized (this) {
                    wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
