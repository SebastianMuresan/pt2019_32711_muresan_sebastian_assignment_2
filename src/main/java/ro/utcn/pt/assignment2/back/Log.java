package ro.utcn.pt.assignment2.back;
/**
 *  This class implements the log where it will be written a message about what happened with the client
 *  */
public class Log {
    public final String queueName;
    public final String message;
    public final Client client;

    /**
     * Constructor that needs (Client, Message, Queue Name)
     * @param  queueName - The name of the service Processor
     * @param  message - what prints in the log when a serviceProcessor dequeues the client
     * @param  client - the client that is being processed
     * */
    public Log(Client client, String message, String queueName) {
        this.client = client;
        this.message = message;
        this.queueName = queueName;
    }
}
