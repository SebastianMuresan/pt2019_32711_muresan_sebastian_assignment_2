package ro.utcn.pt.assignment2.back;


import java.time.LocalTime;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.SECONDS;

/**
 *  This class processes the clients based on their arrivalTime. First client in is the first client served.
 */


public class ServiceProcessor implements Runnable {
    private final BlockingQueue<Client> queue;
    private final Store store;
    private String name;

    /**
     * Using a BlockingQueue because it waits for the queue to become non-empty when retrieving an element,
    and waits for space to become available in the queue when storing an element.

     @param name - name of the Queue (ServiceProcessor or Thread)
     @param queue - BlockingQueue
     @param store - The store to which this ServiceProcessor belongs to
     */

    public ServiceProcessor(String name, BlockingQueue<Client> queue, Store store) {
        this.queue = queue;
        this.name = name;
        this.store = store;
    }

    /**
    * The run method dequeues the queue after the serviceTime has expired and then the Log will notify in which
    * ServiceProcessor it was processed and how much time the client was in queue.
    *
     */
    public void run() {
        try {
            while (this.store.cancelationFlag.get()) {
                Client c = dequeue();
                this.process(c);
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupt");
            System.out.println(e.toString());
        }
    }

    /**
     *Processing the client and then prints to the log when it finished and how much time the client was in queue + the name
     * of the serviceProcessor that processed the client (Notifies all threads)
     *
     * @param c - the client which is being processed
     * */
    public synchronized void process(Client c) throws InterruptedException {
        LocalTime startTime = LocalTime.now();

        this.wait((long)c.serviceTime * 1000);

        LocalTime endTime = LocalTime.now();

        String message = String.format("[%d]: deservit de casa {%s}. [Arrival time]: %s. [Processing started]: %s. [Processing ended]: %s. [Time processed]: %d. [Waited in queue]: %d.", c.getId(), this.name, c.arrivalTime.toString(), startTime.toString(), endTime.toString(), startTime.until(endTime, SECONDS), c.arrivalTime.until(endTime, SECONDS));

        Log log = new Log(c, message, this.name);

        store.GetLog(log);

        notifyAll();
    }

    /**
     *Dequeues the ServiceProcessor and waits if the queue is empty
     *
     * @throws InterruptedException - InterruptedException
     * @return  c - the client which has been processed
     * */
    public synchronized Client dequeue() throws InterruptedException {
        while (queue.isEmpty() && !this.store.cancelationFlag.get())
            wait();

        Client c = queue.take();
        return c;
    }
}
